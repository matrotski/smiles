﻿using UnityEngine;
using System.Collections;
using Mono.Data.Sqlite;
using System.Data;
using System.IO;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class DbController : MonoBehaviour
{
    public SqliteConnection Connection;
    public SqliteCommand Command;
    public SqliteDataReader Reader;
    private string _path;

    public void SetConnection()
    {
        try
        {
            _path = Application.dataPath + "/coinsDb.bytes";
            Connection = new SqliteConnection("URL=file:" + _path);
            Connection.Open();
        }
        catch (Exception)
        {  
        }
    }

    public List<string> GetData()
    {
        var result = new List<string>(); 
        using (Command = new SqliteCommand("SELECT Name FROM Zones", Connection))
        {
            using (Reader = Command.ExecuteReader())
            {
                while (Reader.Read())
                {
                    result.Add(Reader["Name"].ToString());
                }
            } 
        }
        return result;
    }
}
