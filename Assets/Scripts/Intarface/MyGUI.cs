﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class MyGUI : MonoBehaviour {
	public Score sc;
	public Texture tx;
    public GameObject Live;
	public GameObject Points;
    public TextMesh tm;
    public GameObject smile;
    public GameObject LeftBorder;

    private GUIStyle gs;
	private GameObject[] Scalepans;
    private GameObject TheEnd;
	//private float volMusic = Global.volMusic;
	//private float volSound = Global.volSound;
	private AudioSource[] AOAS;

	private const int PADDING = 10;
	private const int LABEL_WIDTH = 120;
	private const int LABEL_HEIGHT = 40;

	private Vector3 firstPosition;

    //"llHeo world ||:)||:)||:) He||:)llo wo||:)rld ||:)";

    private string mainStr = "One of Sigmund Freud's early ||:)patients rushed out into an ||:)||:)Austrian afternoon on her ||:)way to meet her best friend at a||:)||:) coffee house. Over Cappuccino||:)||:)||:) and Viennese pastries, she suddenly||:)||:) burst out crying. Her friend begged her to share||:)||:)||:) what was wrong. Oh, it's just terrible,||:)||:) she wailed. Today the||:)||:)||:) doctor told me Im in love with my father, and. . .and. . .and you ||:)||:)||:)know, hes a married man!||:)";

    private string newMainString = "";

    private Vector3 leftPosition;
    private Font font;

    private GUIStyle gs1;

    private Vector2 offset;

    public GameObject mainSmile;

    bool canMove = true;
    bool leftDown = false;
    bool rightDown = false;
    float maxSpeedWorld = 0.007f;
    float maxSpeedScreen;

    void Start () {
		//gs = new GUIStyle();
		AOAS = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
		Scalepans = GameObject.FindGameObjectsWithTag ("Scalepan");
        TheEnd = GameObject.FindGameObjectWithTag("TheEnd");
        //Life = GameObject.FindGameObjectWithTag("Life");
        firstPosition = Camera.main.ScreenToWorldPoint(Live.transform.position);

        leftPosition = LeftBorder.transform.position;

        //tm = new TextMesh();
        //tm.text = "lkjkjglkhf";

        //sc.LivesChange += (o, e) =>
        //{
        //for(var i = 0; i < Lives.Length - sc.Lives; ++i)
        //{
        //Lives[i].SetActive(false);
        //}
        //};
        
        var list = GUIHelper.GetAllItems(mainStr);
        newMainString = "";
        font = (Font)Resources.Load("Fonts/Unispace", typeof(Font));
        var startPosition = 0;
        foreach (var item in list)
        {
            newMainString += mainStr.Substring(startPosition, item.Position - startPosition);
            var itemPosition = Camera.main.ScreenToWorldPoint(new Vector3(GUIHelper.GetFirstItemPosition(newMainString, font), Screen.height - LABEL_HEIGHT/2, 0));
            
            
            for (var i = 0; i < 2 * item.SymbolCount; ++i)
                newMainString += " ";
            startPosition = item.Position + 2 * item.SymbolCount;
            
            var go = Instantiate(smile, new Vector3(itemPosition.x, itemPosition.y, 0), Quaternion.identity, mainSmile.transform);
            go.transform.localScale = new Vector3(0.5f, 0.5f);
        }
        maxSpeedScreen = Mathf.Abs(Camera.main.WorldToScreenPoint(new Vector3(maxSpeedWorld, 0)).x - Camera.main.WorldToScreenPoint(new Vector3(0, 0)).x);
    }
	
	// Update is called once per frame
	void Update () {
        ///if (Input.GetKeyDown(KeyCode.Tab)){
        //if (!Global.isPause) Global.isPause = true;
        //else
        //Global.isPause = false;
        //myPause(Global.isPause);
        //}
        //if (sc.model != null && sc.model.TheEnd)
        //{
            //TheEnd.SetActive(true);
        //}
		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
        var ScreenSize = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
		Live.transform.position -= new Vector3(Screen.width - (Screen.width * sc.Lives / 300), 0, 0);//Camera.main.WorldToScreenPoint(new Vector3 (firstPosition.x - Screen.width + (Screen.width * sc.Lives / 300), firstPosition.y, 0));
        //if (canMove && leftDown)
        //{
        //    mainSmile.transform.position = new Vector2(mainSmile.transform.position.x - maxSpeedWorld, mainSmile.transform.position.y);
        //}
        //if (canMove && rightDown)
        //{
        //    mainSmile.transform.position = new Vector2(mainSmile.transform.position.x + maxSpeedWorld, mainSmile.transform.position.y);
        //}
    }
	void OnGUI()
	{
		//if (sc.theEnd){
			//GUI.Label(new Rect(Screen.width/2-20,Screen.height/2-10,100,20),"The End");
			//Global.move = false;
			//Global.points = sc.points;
		//}
		//if (GUI.Button(new Rect(10, 10, 100, 20),"Exit level"))
		//{
			//Application.Quit();
			//Application.LoadLevel("ChangeZoneMenu");
		//}

		if (GUI.Button(new Rect(10, 50, 30, 30),"<"))
		{
			if (Time.timeScale > 0)
				Time.timeScale -= 0.1f;
		}

		if (GUI.Button(new Rect(45, 50, 30, 30),">"))
		{
			if (Time.timeScale < 1)
				Time.timeScale += 0.1f;
		}
		gs = new GUIStyle(GUI.skin.label);
		gs.alignment = TextAnchor.MiddleCenter;

		for (int i = 0; i < sc.Scalepans.Length; ++i) {
			var screenPosition = Camera.main.WorldToScreenPoint (Scalepans[i].transform.position);
			var _position = new Vector2 (screenPosition.x, Screen.height - screenPosition.y - 5 * PADDING);
			var rect = new Rect(_position.x, _position.y - 2 * LABEL_HEIGHT, LABEL_WIDTH, LABEL_HEIGHT);

			GUI.Label (rect, sc.Scalepans[i].ToString(), gs);
		}

        gs1 = new GUIStyle(GUI.skin.label);
        gs.wordWrap = false;
        gs1.contentOffset += offset;
        gs1.font = font;
        gs1.fontSize = 14;

        GUI.Label(new Rect(PADDING, PADDING, LABEL_WIDTH, LABEL_HEIGHT),"Lives: " + sc.Lives.ToString());
		GUI.Label(new Rect(Screen.width - LABEL_WIDTH - PADDING, PADDING, LABEL_WIDTH, LABEL_HEIGHT),"Score: " + sc.points.ToString());
		GUI.Label(new Rect(Screen.width- 2 * LABEL_WIDTH - 2 * PADDING, PADDING, LABEL_WIDTH, LABEL_HEIGHT),"Time: " + sc.tmin.ToString()+":"+sc.tsec.ToString());
        //GUI.Label(new Rect(0, Screen.height - LABEL_HEIGHT, 10*Screen.width, LABEL_HEIGHT), newMainString, gs1);
        GUI.Box(new Rect(0, LABEL_HEIGHT, 10*Screen.width, LABEL_HEIGHT), newMainString, gs1);

        if (canMove && leftDown)
        {
            offset += new Vector2(-maxSpeedScreen, 0);
            mainSmile.transform.position = new Vector2(mainSmile.transform.position.x - maxSpeedWorld, mainSmile.transform.position.y);
        }
        if (canMove && rightDown)
        {
            offset += new Vector2(maxSpeedScreen, 0);
            mainSmile.transform.position = new Vector2(mainSmile.transform.position.x + maxSpeedWorld, mainSmile.transform.position.y);
        }

        offset += new Vector2(-maxSpeedScreen, 0);
        mainSmile.transform.position = new Vector2(mainSmile.transform.position.x - maxSpeedWorld, mainSmile.transform.position.y);
        /*if (Global.isPause)
		{

			GUILayout.BeginArea(new Rect(10, 10, Screen.width,Screen.height), tx, gs);
			volMusic = GUI.HorizontalSlider(new Rect(120, 10, 100, 20),volMusic,0f,1f);
			if (GUI.Button(new Rect(10, 10, 100, 20),"Exit"))
			{
				Global.isPause = false;
				Application.Quit();
			}

			Global.volMusic = volMusic;
			Camera.main.audio.volume = Global.volMusic;
			int a = (int)(Camera.main.audio.volume*10);
			GUI.Label(new Rect(120, 15, 100, 20),a.ToString());
			GUILayout.EndArea();
		}*/
    }

	public void myPause(bool isPause)
	{
		if (isPause){
			Time.timeScale = 0;
			for (int i = 0; i < AOAS.Length; i++)
			{
				AOAS[i].Pause();
				//AOAS[i].enabled = false;
			}
		}
		else{
			Time.timeScale = 1;				
			for (int i = 0; i < AOAS.Length; i++)
			{
				//AOAS[i].enabled = true;
				AOAS[i].Play();
			}
		}
	}

    public void OnLeftButtonDown()
    {
        rightDown = false;
        leftDown = true;
    }

    public void OnRightButtonDown()
    {
        leftDown = false;
        rightDown = true;
    }

    public void OnLeftButtonUp()
    {
        leftDown = false;
    }

    public void OnRightButtonUp()
    {
        rightDown = false;
    }
}