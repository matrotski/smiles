﻿using UnityEngine;
using System.Collections;
using System;

public class Score : MonoBehaviour {

	public WaterZoneViewModel model;

    public event EventHandler LivesChange;

    private float lives;
	public float Lives
    {  
		get
        {
            return lives;
        } 
		set
        {
            if (lives != value)
            {
                lives = value;
                //OnLivesChange();
            }
        } 
	}

	float decriseLiveCoef = 1f;

	public int[] Scalepans { 
		get
		{
			return scalepans;
		}
	}

	private int[] scalepans;

	public Vector2 FTime;
	public Vector2 time
	{
		set
		{
			timef = value.x*60+value.y;
		}
	}

	public int points
	{
		get
		{
			return pointsF;
		}
	}

	public int tsec
	{
		get
		{
			return tsecf;
		}
	}
	public int tmin
	{
		get
		{
			return tminf;
		}
	}

	private int LeftPoints;
	private int RightPoints;

	private int pointsF;
	private float timef; 
	private int tminf = 0, tsecf = 0;

	// Use this for initialization
	void Start () {
		lives = 300;//GameObject.FindGameObjectsWithTag("Life").Length;
		pointsF = 0;
		timef = FTime.x*60+FTime.y;

		model = gameObject.GetComponent<WaterZoneViewModel>();

		var ScalepansLength = GameObject.FindGameObjectsWithTag ("Scalepan").Length;
		scalepans = new int[ScalepansLength];
		for (int i = 0; i < ScalepansLength; ++i)
			scalepans [i] = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (!model.TheEnd)
        {
            timef -= Time.deltaTime;
            if (tminf > 60)
                tminf = 0;
            else
                tminf = (int)timef / 60;
            tsecf = (int)timef % 60;
			SetLives();
            SetHeap();
        }
        endGame();
    }
	public void endGame()
	{
		if (timef <= 0 || Lives <= 0)
            model.TheEnd = true;
	}

	public void SetScalepanPoints(int ScalepanIndex, int Points)
	{
		if (ScalepanIndex >= 0 && ScalepanIndex < Scalepans.Length) {
			scalepans [ScalepanIndex] += Points;
			decriseLiveCoef = (float)MaxMin(scalepans, 1) / (float)MaxMin(scalepans, 0);
			for (int i = 1; i < scalepans.Length; ++i) {
				if (scalepans [i] == scalepans [i - 1])
					pointsF += scalepans [i];
				else
					break;
			}
		}
	}

    public void OnLivesChange()
    {
        if (LivesChange != null)
            LivesChange(this, null);
    }

	private int MaxMin(int[] arr, int sign)
	{
		int buffer = arr[0];
		for (int i = 1; i < arr.Length; ++i) {
			if (sign == 0)
				if (arr[i] < buffer)
				{
					buffer = arr[i];
				}
			else
				if (arr[i] > buffer)
				{
					buffer = arr[i];
				}
		}
		if (buffer == 0)
			buffer = 1;
		return buffer;
	}

	private void SetLives()
	{
		if (decriseLiveCoef != 1f) {
			lives -= decriseLiveCoef * Time.deltaTime;
		}
	}

	private void SetHeap()
	{
        if (model.IsLoaded)
        {
            if (pointsF >= 40 && pointsF < 80)
                model.Heap = 2;
            else
                if (pointsF >= 80 && pointsF < 150)
                model.Heap = 3;
            else
                if (pointsF >= 150)
                model.Heap = 4;
            else
                model.Heap = 1;
        }
	}
}
