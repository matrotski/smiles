﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Tools;
using System.Collections.Generic;

public class RandomCoin : MonoBehaviour {

	public WaterZoneViewModel model;

	System.Random rndPosition;
	System.Random rndCoin;
    System.Random rndMoment;
    int coinIndex = 0;
	IList<int> oldCoinsIndex;
    int coef = 4;
    // Use this for initialization
    void Start () {
		rndCoin = new System.Random ();
		rndPosition = new System.Random ();
        rndMoment = new System.Random();
        //model = GetComponent<WaterZoneViewModel>();
        oldCoinsIndex = new List<int>();
	}
	
	void Update () {
        if (!model.TheEnd)
        {
            //if (model.IsLoaded && WaterZoneViewModel.CurrentCoinsCount <= 0)
            if (WaterZoneViewModel.CurrentCoinsCount / model.Coins.Length < 0.5f)
                coef = 4;
            if /*(WaterZoneViewModel.CurrentCoinsCount < model.Coins.Length*/ (rndMoment.Next(0,200) < coef)
            //if (model.IsLoaded && WaterZoneViewModel.CurrentCoinsCount < model.Heap)
            {
                //for (int i = 0; i < model.Heap; ++i)
                {
                    int position = rndPosition.Next(0, model.Places.Count);
                    coinIndex = rndCoin.Next(0, model.Coins.Length);
                    while (oldCoinsIndex.Contains(coinIndex))
                    {
                        coinIndex = rndCoin.Next(0, model.Coins.Length);
                    }

                    oldCoinsIndex.Add(coinIndex);

                    model.Coins[coinIndex].Obj.transform.position = model.Places[position].Obj.transform.position;
                    model.Coins[coinIndex].Obj.SetActive(true);
                    model.Coins[coinIndex].Place = position;

                    WaterZoneViewModel.CurrentCoinsCount++;
                    if (WaterZoneViewModel.CurrentCoinsCount / model.Coins.Length > 0.5f)
                        coef = 3;
                    if (WaterZoneViewModel.CurrentCoinsCount / model.Coins.Length > 0.7f)
                        coef = 2;
                    if (WaterZoneViewModel.CurrentCoinsCount / model.Coins.Length > 0.8f)
                        coef = 2;
                    if (WaterZoneViewModel.CurrentCoinsCount / model.Coins.Length > 0.9f)
                        coef = -1;
                    if (WaterZoneViewModel.CurrentCoinsCount / model.Coins.Length > 0.5f)
                        coef = 3;
                    if (WaterZoneViewModel.CurrentCoinsCount / model.Coins.Length > 0.7f)
                        coef = 2;
                    if (WaterZoneViewModel.CurrentCoinsCount / model.Coins.Length > 0.8f)
                        coef = 2;
                    

                }
                oldCoinsIndex.Clear();
            }
		}
	}	
}