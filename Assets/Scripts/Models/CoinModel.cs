﻿using UnityEngine;

public class Coin
{
	public Coin()
	{
		Place = -1; 
	}
	public GameObject Obj { get; set; }
	public int Value;
	public int Place { get; set; }
}