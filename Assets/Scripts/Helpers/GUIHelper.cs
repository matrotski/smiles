﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GUIHelper
{ 
    public static int GetFirstItemPosition(string str, Font font)
    {
        int width = 0;
        foreach (var c in str)
            width += GetSymbolWidth(font, c);
        return width;
    }

    public static List<StringItemModel> GetAllItems(string str)
    {
        //var dict = new Dictionary<int, string>();
        var listItems = new List<StringItemModel>();
        for (var i = 0; i < str.Length - 1; ++i)
            //if (str[i] == '|' && str[i + 1] != '|')
            //dict.Add(i, str[i + 1].ToString());
            //else
            if (str[i] == '|' && str[i + 1] == '|')
                listItems.Add(new StringItemModel { Position = i + 2, SymbolCount = 2, Symbol = (str[i + 2] + str[i + 2 + 1]).ToString() });
                //dict.Add(i, (str[i + 2] + str[i + 3]).ToString());

                //return dict;
        return listItems;
    }

    private static int GetSymbolWidth(Font font, char c)
    {
        var symbol = font.characterInfo.Where(s => s.index == (int)c).FirstOrDefault();
        Debug.Log("ghfgh "+symbol.maxX.ToString() + " " + symbol.advance.ToString());
        return symbol.advance;
    }
}

public class StringItemModel
{
    public int Position { get; set; }
    public int SymbolCount { get; set; }
    public string Symbol { get; set; }
}
