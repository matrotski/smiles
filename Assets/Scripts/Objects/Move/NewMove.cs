﻿using UnityEngine;
using System.Collections;

public class NewMove : MonoBehaviour {

	public GameObject LeftBorder;
	public GameObject RightBorder;

	bool canMove = true;
    bool leftDown = false;
    bool rightDown = false;
	float maxSpeed = 0.3f;

    public void Start()
    {
    }

    public void Update()
    {
        if(canMove && leftDown /*&& transform.position.x > LeftBorder.transform.position.x*/)
            transform.position = new Vector2(transform.position.x - maxSpeed, transform.position.y);
        if (canMove && rightDown /*&& transform.position.x < RightBorder.transform.position.x*/)
            transform.position = new Vector2(transform.position.x + maxSpeed, transform.position.y);
    }

	public void OnLeftButtonDown()
	{
        rightDown = false;
        leftDown = true;
	}

	public void OnRightButtonDown()
	{
        leftDown = false;
        rightDown = true;
    }

    public void OnLeftButtonUp()
    {
        leftDown = false;
    }

    public void OnRightButtonUp()
    {
        rightDown = false;
    }
}
