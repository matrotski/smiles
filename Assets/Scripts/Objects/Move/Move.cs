﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

	public GameObject LeftBorder;
	public GameObject RightBorder;

	bool canMove = true;
	float maxSpeed = 0.2f;

	public Jostic jst;

	void Start()
	{
	}

	// Update is called once per frame
	void Update () {
		
		if (canMove) {
			if (jst.LeftButtonDown && transform.position.x > LeftBorder.transform.position.x) {
				transform.position = new Vector2 (transform.position.x - maxSpeed, transform.position.y);
			} else
			if (jst.RightButtonDown && transform.position.x < RightBorder.transform.position.x) {
				transform.position = new Vector2 (transform.position.x + maxSpeed, transform.position.y);
			}
		}
			    
	}
}
