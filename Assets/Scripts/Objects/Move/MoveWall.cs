﻿using UnityEngine;
using System.Collections;

public class MoveWall : MonoBehaviour {

	public GameObject leftBorder;
	public GameObject rightBorder;

	private int direction = 1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (transform.position.x < leftBorder.transform.position.x)
			direction = 1;
		else
			if (transform.position.x > rightBorder.transform.position.x)
				direction = -1;
		transform.position += new Vector3(direction * 0.1f,0,0); 
	}
}
