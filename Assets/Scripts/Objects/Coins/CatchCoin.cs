﻿using UnityEngine;
using System.Collections;

public class CatchCoin : MonoBehaviour {

	public Score sc;

	public int ScalepanIndex;

    public void OnTriggerEnter2D(Collider2D coll)
    {
		if (coll != null) {
			if (coll.transform.parent.parent.tag == "Coins")
			{
			    var coinValue = 1;
                if(!int.TryParse(coll.transform.parent.tag, out coinValue))
                    coinValue = 1;
                sc.SetScalepanPoints(ScalepanIndex, coinValue);
            }
		}
    }
    
}
