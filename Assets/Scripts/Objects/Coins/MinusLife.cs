﻿using UnityEngine;
using System.Collections;

public class MinusLife : MonoBehaviour {

	public Score sc;
	
	public void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll != null) {
			if (coll.transform.parent.parent.tag == "Coins") {
                if (sc.Lives > 0)
				    sc.Lives -= 50;			
			}
		}
	}
}
