﻿using UnityEngine;
using System.Collections;

public class DestroyCoin : MonoBehaviour {

	// Use this for initialization
	//public AudioClip myClip;
	public WaterZoneViewModel model;
	public float delay;

    public void OnTriggerEnter2D(Collider2D coll)
    {
		if (coll != null) {
			if (coll.transform.parent.parent.tag == "Coins") {
				coll.gameObject.SetActive(false);
				coll.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2 (0, 0);
				coll.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
				coll.gameObject.transform.position = Vector3.zero;

				if (WaterZoneViewModel.CurrentCoinsCount > 0)
                    WaterZoneViewModel.CurrentCoinsCount--;
			}
		}
    }

    
}
