using UnityEngine;

public class WaterZoneManager
{	
	public Coin[][] GetCoins(out int length)
	{
        length = 0;
	    var CoinsGroups = GameObject.FindGameObjectWithTag("Coins").transform;
        var GroupCoinsCount = CoinsGroups.childCount;
		Coin[][] Coins = new Coin[GroupCoinsCount][];
		for (var i = 0; i < GroupCoinsCount; ++i)
		{
		    var CoinsGroup = CoinsGroups.GetChild(i);//GameObject.FindGameObjectWithTag ("Coins" + (i+1).ToString());
            var CoinsGroupLength = CoinsGroup.childCount;
			Coins[i] = new Coin[CoinsGroupLength];
			length += CoinsGroupLength;
			for (var j = 0; j < CoinsGroupLength; ++j)
			{
                Coins[i][j] = new Coin();
                Coins [i][j].Obj = CoinsGroup.GetChild(j).gameObject;
                var coinValue = 1;
                int.TryParse(CoinsGroup.tag, out coinValue);
                if (!int.TryParse(CoinsGroup.tag, out coinValue))
                    coinValue = 1;
                Coins[i][j].Value = coinValue;
            }
		}
		return Coins;
	}

	public GameObject[] GetPlases()
	{
		return GameObject.FindGameObjectsWithTag ("Place");
	}
}


