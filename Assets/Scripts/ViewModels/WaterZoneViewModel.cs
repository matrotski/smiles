﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaterZoneViewModel: MonoBehaviour {

	private WaterZoneManager waterZoneManager;
	
	public Coin[] Coins { get; set; }
	private int coinsLength;
	public IList<Place> Places { get; set; }

    public bool IsLoaded { get; set; }
	
	public static int CurrentCoinsCount { get; set; }
	public int Heap { get; set; }

    private bool endGameF = false;
    public bool TheEnd
    {
        get
        {
            return endGameF;
        }
        set
        {
            if (endGameF != value)
                endGameF = value;
        }
    }

    public void Start()
	{
		waterZoneManager = new WaterZoneManager ();
        IsLoaded = false;
        SetValues (waterZoneManager.GetCoins (out coinsLength), waterZoneManager.GetPlases ());
	}

	public void SetValues(Coin[][] coins, GameObject[] plases)
	{
		
		Coins = new Coin[coinsLength];
		Places = new List<Place> ();

		int k = 0;
		for (int i = 0; i < coins.Length; ++i) {
			for (int j = 0; j < coins[i].Length; ++j) {
                Coins[k] = new Coin();
                Coins[k].Obj = coins[i][j].Obj;
				Coins[k].Value = i;
                k++;
			}
		}

		for (int i = 0; i< plases.Length; ++i) {
			Place po = new Place();
			po.Obj = plases[i];
			Places.Add(po);
		}

		CurrentCoinsCount = 0;
		Heap = 1;

		for (int i = 0; i< Places.Count; ++i) {
			Places [i].Obj.SetActive(false);
		}
        for (int i = 0; i < Coins.Length; ++i)
        {
            Coins[i].Obj.SetActive(false);
            Coins[i].Obj.transform.position = Vector3.zero;
        }
        IsLoaded = true;
    }
}
