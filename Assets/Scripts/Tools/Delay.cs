﻿using UnityEngine;

namespace Assets.Scripts.Tools
{
    class Timer
    {
        public static void Delay(float ms)
        {
            var time = ms;
            while (true)
            {
                time -= Time.deltaTime;
                if (time <= 0) break;
            }
        }
    }
}
